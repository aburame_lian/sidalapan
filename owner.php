	<<?php session_start() ?>

	<!DOCTYPE html>
	<!-- saved from url=(0074)https://fooplugins.github.io/FooTable/docs/examples/component/sorting.html -->
	<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!--<link rel="icon" href="../../favicon.ico">-->

	<title>Sistem Informasi Pelanggan Telkom Banyuwangi</title>

	<!-- Bootstrap core CSS -->
	<link href="./css/bootstrap.min.css" rel="stylesheet">
	<link href="./css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Prism -->
	<link href="./css/prism.css" rel="stylesheet">

	<!-- FooTable Bootstrap CSS -->
	<link href="./css/footable.bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="./css/docs.css" rel="stylesheet">
	<script src="./js/circles-master/circles.js"></script>
	<title>Circles</title>
	<style>
		#canvas .circle {
			display: inline-block;
			margin: 1em;
		}
		.circles-decimals {
			font-size: .4em;
		}
	</style>

	<script src="./js/demo-rows.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<?php
		include ('conf/conn.php');
		include ('content/header.php');
		
		$sql = "SELECT DP,COUNT(DP) as total from plg_banyuwangi GROUP BY DP";
		$result = mysqli_query($con, $sql)  or die('Query fail: Gagal koneksi DB');
		$sql2 = "SELECT (SELECT COUNT(STATUS2) FROM `plg_banyuwangi` WHERE STATUS2 ='PROCESSED') /
		(SELECT COUNT(STATUS2) FROM `plg_banyuwangi` WHERE STATUS2 ='NOT UPGRADED' AND SNR_DOWN > 20)";
		$result1 = mysqli_query($con, $sql2)  or die('Query fail: Gagal koneksi DB');

		

		?>
	</head>

	<body class="docs" class="margin-top-60">

		
		<!-- Content -->
		<div class="container">
			<div class="callout callout-info">
				<h4>Catatan</h4>
				<ul>
					<li>
						Aplikasi ini masih dalam tahap pengembangan dan masih dalam bentuk prototype.
					</li>
					<li>
						Semua fitur yang ada masih dalam proses pengembaangan dan akan diimplementasikan lagi.
					</li>
					<li>
						Masukan dan saran diperlukan untuk pengembangan sistem informasi ini.
					</li>
				</ul>
			</div>

			<ul class="nav nav-tabs">
				<li class="active" ><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
				<li><a href="#search" data-toggle="tab">Search</a></li>
				<li><a href="#data" data-toggle="tab">Data</a></li>
				<li><a href="#insert" data-toggle="tab">Masukan</a></li>
			</ul>
			<div class="tab-content">

				<div class="tab-pane active" id="dashboard">
					
					<h3><b>Dashboard</b></h3>
					<p>
						Halaman ini menampilkan statistik dalam bentuk chart dari pelanggan Telkom Banyuwangi. Statistik meliputi tab dibawah ini:
					</p>
					
					<script src="js/highcharts.js"></script>
					<script src="js/highcharts-3d.js"></script>
					<script src="js/modules/exporting.js"></script>
					<script type="text/javascript">
						document.getElementById("dash").setAttribute("class","active");
					</script>    
					<br></br>	

					
					<div class="col-md-8">
						<h4><center><i>5 besar Grafik SNR Pelanggan 2P</i></center></h4>
						
						<div id="overall" class="chart"></div> <!-- Container for Chart A -->
						<div class="spacer"></div>
						<div id="pie" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div><!-- Container for Chart B -->
						
					</div>
					<div class="col-md-4">
						<div class="jumbotron" id="overall">
							<center><h4><i>Progress Bar </center></h4>
							(Pelanggan yang diproses dibandingkan pelanggan yang belum diproses dengan SNR > 20)</i>	</center>
							<div id="canvas">
								<div class="circle" id="circles-1">
								</div>
							</div>

							<div class="example">
								<h4><center><i>Jumlah ODP</i></center></h4>
								<table id="datamain" class="table footable footable-1 breakpoint-lg" data-filtering="true" data-paging="true" data-sorting="true" style="display: table;">
									<thead>
										<tr>

											<th data-breakpoints="xs">Nama ODP</th>
											<th data-breakpoints="xs">Jumlah ODP</th>									
										</tr>

									</thead>
									<tbody>
										<?php while ($row = mysqli_fetch_array($result)) { 
											echo "<tr>";
											echo "<td>" . $row[0] . "</td>\n";
											echo "<td>" . $row[1] . "</td>\n";									
											echo "</tr>" ;
										}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					

				</div>
				<div class="tab-pane" id="search">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<h3><b>Pencarian</b></h3>
						<p>
							Silahkan memasukan data yang ingin dicari sesuai dengan spesifikasi dibawah ini:
						</p>
						<form method="post" action="search.php">
							<div class="form-group ">
								<label class="control-label " for="nama">
									Nama Pelanggan
								</label>
								<input class="form-control" id="nama" name="nama" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="snrmin">
									Nilai SNR Minimum (Downstream)
								</label>
								<select class="select form-control" id="snrmin" name="snrmin">
									<option value="10.0">
										10
									</option>
									<option value="15.0">
										15
									</option>
									<option value="20.0">
										20
									</option>
								</select>
							</div>
							<div class="form-group ">
								<label class="control-label " for="snrmax">
									Nilai SNR Maximum (Downstream)
								</label>
								<select class="select form-control" id="snrmax" name="snrmax">
									<option value="25">
										25
									</option>
									<option value="30">
										30
									</option>
									<option value="35">
										35
									</option>
									<option value="40">
										40
									</option>
								</select>
							</div>
							<div class="form-group ">
								<label class="control-label " for="location">
									Lokasi
								</label>
								<input class="form-control" id="location" name="location" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="odp">
									Kode ODP
								</label>
								<input class="form-control" id="odp" name="odp" type="text"/>
							</div>
							<div class="form-group">
								<div>
									<button class="btn btn-primary " name="submit" type="submit">
										Submit
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="tab-pane" id="data">
					<div class="example">
						<table id="showcase-example-1" class="table" data-paging="true" data-filtering="true" data-sorting="true" data-editing="true"></table>
					</div>
					<!-- Editing Modal Markup -->
					<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
						<style scoped>
							/* provides a red astrix to denote required fields - this should be included in common stylesheet */
							.form-group.required .control-label:after {
								content:"*";
								color:red;
								margin-left: 4px;
							}
						</style>
						<div class="modal-dialog" role="document">
							<form class="modal-content form-horizontal" id="editor" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
									<h4 class="modal-title" id="editor-title">Add Row</h4>
								</div>
								<div class="modal-body">
									<input type="number" id="id" name="id" class="hidden"/>
									<div class="form-group required">
										<label for="nama" class="col-sm-3 control-label">Nama</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
										</div>
									</div>
									<div class="form-group required">
										<label for="snrup" class="col-sm-3 control-label">SNR Upstream</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="snrup" name="snrup" placeholder="SNR Upstream">
										</div>
									</div>
									<div class="form-group">
										<label for="snrdown" class="col-sm-3 control-label">SNR Downstream</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="snrdown" name="snrdown" placeholder="SNR Downstream">
										</div>
									</div>
									<div class="form-group required">
										<label for="odp" class="col-sm-3 control-label">ODP</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="odp" name="odp" placeholder="ODP">
										</div>
									</div>
									<div class="form-group">
										<label for="alamat" class="col-sm-3 control-label">Alamat</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
										</div>
									</div>
									<div class="form-group">
										<label for="abonemen" class="col-sm-3 control-label">Abonemen</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="abonemen" name="abonemen" placeholder="Abonemen">
										</div>
									</div>
									<div class="form-group">
										<label for="cp" class="col-sm-3 control-label">Contact Person</label>
										<div class="col-sm-9">
											<input type="number" class="form-control" id="cp" name="cp" placeholder="Contact Person">
										</div>
									</div>
									<div class="form-group">
										<label for="geotag" class="col-sm-3 control-label">Geotag</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="geotag" name="geotag" placeholder="Geotag">
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label " for="status">
											Status
										</label>
										<div class="col-sm-9">
											<select class="select form-control" id="status" name="status">
												<option value="Upgraded">
													Upgraded
												</option>
												<option value="Processed">
													Processed
												</option>
												<option value="Not Upgraded">
													Not Upgraded
												</option>
											</select>
										</div>
									</div>

								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Save changes</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</div>
							</form>
						</div>
					</div>
					<div class="callout callout-info">
						<h4>Note</h4>
						<p>Columns with HTML or Date data can make use of the <a href="https://fooplugins.github.io/FooTable/docs/components/sorting.html#cell-sortValue"><code>data-sort-value</code></a> attribute on a cell to supply a different value to use when sorting.</p>
					</div>
				</div>
				<div class="tab-pane" id="insert">
					<h3><b>Entry Data Baru</b></h3>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p>Masukan data baru yang akan dimasukan ke dalam database pelanggan consumer Service PT. Telkom Banyuwangi</p>

						<form method="post" action="insert.php">

							<div class="form-group ">
								<label class="control-label " for="sndid">
									SND ID
								</label>
								<input class="form-control" id="sndid" name="sndid" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="name">
									Nama
								</label>
								<input class="form-control" id="name" name="name" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="snrup">
									SNR_UP
								</label>
								<input class="form-control" id="snrup" name="snrup" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="snrdown">
									SNR_DOWN
								</label>
								<input class="form-control" id="snrdown" name="snrdown" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="odp">
									ODP
								</label>
								<input class="form-control" id="odp" name="odp" type="text"/>
							</div>
							<div class="form-group ">
								<label class="control-label " for="address">
									Alamat
								</label>
								<input class="form-control" id="address" name="address" type="text"/>
							</div>
							<div class="form-group">
								<div>
									<button class="btn btn-primary " name="submit" type="submit">
										Submit
									</button>
								</div>
							</div>
						</form>
					</div>
					
				</div>
			</div> <!-- /container -->
			<div class="docs-section">

				
			</div>
			

		</div>

		<!-- Placed at the end of the document so the pages load faster -->
		<script src="./js/jquery.min.js"></script>
		<script src="./js/bootstrap.min.js"></script>
		<script src="./js/prism.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./js/ie10-viewport-bug-workaround.js"></script>
		<!-- Add in any FooTable dependencies we may need -->
		<script src="./js/moment.min.js"></script>
		<!-- Add in FooTable itself -->
		<script src="./js/footable.js"></script>
		<!-- Initialize FooTable -->
		<script>
				/*
				jQuery(function($){
					$('#showcase-example-1').footable({
						"columns": $.get('content/column.json'),
						"rows": <?php include ('content/row.php') ?>
					});
				});*/

				jQuery(function($){
					var $modal = $('#editor-modal'),
					$editor = $('#editor'),
					$editorTitle = $('#editor-title'),
					ft = FooTable.init('#showcase-example-1', {
						columns: $.get("content/column.json"),
						rows: <?php include ('content/row.php') ?>,
						editing: {
							addRow: function(){
								$modal.removeData('row');
								$editor[0].reset();
								$editorTitle.text('Add a new row');
								$modal.modal('hide');
							},
							editRow: function(row){
								var values = row.val();
								$editor.find('#id').val(values.id);
								$editor.find('#nama').val(values.nama);
								$editor.find('#snrup').val(values.snrup);
								$editor.find('#snrdown').val(values.snrdown);
								$editor.find('#odp').val(values.odp);
								$editor.find('#alamat').val(values.alamat);
								$editor.find('#abonemen').val(values.abonemen);
								$editor.find('#cp').val(values.cp);
								$editor.find('#geotag').val(values.geotag);
								$editor.find('#status').val(values.status);
								$modal.data('row', row);
								$editorTitle.text('Edit row #' + values.id);
								$modal.modal('show');

							},
							deleteRow: function(row){
								if (confirm('Are you sure you want to delete the row?')){
									row.delete();
								}
							}
						}

					}),
					uid = 10001;
					$editor.on('submit', function(e){
						if (this.checkValidity && !this.checkValidity()) return;
						e.preventDefault();
						var row = $modal.data('row'),
						values = {
							id: $editor.find('#id').val(),
							nama: $editor.find('#nama').val(),
							snrup: $editor.find('#snrup').val(),
							snrdown: $editor.find('#snrdown').val(),
							odp: $editor.find('#odp').val(),
							alamat: $editor.find('#alamat').val(),
							abonemen: $editor.find('#abonemen').val(),
							cp: $editor.find('#cp').val(),
							geotag: $editor.find('#geotag').val(),
							status: $editor.find('#status').val()

						};

						if (row instanceof FooTable.Row){
							row.val(values);
							$.post( "update.php", values );
						} else {
							values.id = uid++;
							ft.rows.add(values);
						}
						$modal.modal('hide');
					});

				}); 
			</script>


			<script type="text/javascript">
				$(function () {
					var chart;
					var chart2;
					$('document').ready(function() {
						$.getJSON("content/r1.php", function(json) {
							$.getJSON("content/r2.php", function(json2){
								chart	= new Highcharts.Chart({
									chart: {
										renderTo: 'overall',
										type: 'column',
										options3d: {
											enabled: true,
											alpha: 15,
											beta: 15,
											depth: 50,
											viewDistance: 25

										}
									},
									title: {
										text: 'Chart Top Consumer dengan nilai SNR Terbesar'
									},
									subtitle: {
										text: 'Grafik ini menjelaskan kustomer dengan nilai SNR tertinggi'
									},
									xAxis: {
										categories: json
									},

									plotOptions: {
										column: {
											depth: 25
										}
									},
									series: [{
										name:'SNR Margin',
										data: json2,color: '#cc0000'
									}]
								});
								chart2= new Highcharts.Chart({
									chart: {
										renderTo: 'pie',
										plotBackgroundColor: null,
										plotBorderWidth: null,
										plotShadow: false,
										type: 'pie'
									},
									title: {
										text: 'Browser market shares January, 2015 to May, 2015'
									},
									tooltip: {
										pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
									},
									plotOptions: {
										pie: {
											allowPointSelect: true,
											cursor: 'pointer',
											dataLabels: {
												enabled: true,
												format: '<b>{point.name}</b>: {point.percentage:.1f} %',
												style: {
													color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
												}
											}
										}
									},
									series: [{
										colorByPoint: true,
										data: <?php include ('content/row1.php')?>
									}]
								});

								function showValues() {
									$('#alpha-value').html(chart.options.chart.options3d.alpha);
									$('#beta-value').html(chart.options.chart.options3d.beta);
									$('#depth-value').html(chart.options.chart.options3d.depth);
								}



								$('#sliders input').on('input change', function () {
									chart.options.chart.options3d[this.id] = this.value;
									showValues();
									chart.redraw(false);
								});

								showValues();
							});
						});
					});

				});	


				jQuery(function($){
					$('#datamain').footable();

				});

			</script>
			
			<script>
		//@ http://jsfromhell.com/array/shuffle [v1.0]
		function shuffle(o){ //v1.0
			for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
				return o;
		}
		var colors = [
		['#F4BCBF', '#D43A43']
		], circles = [];
		for (var i = 1; i <= 5; i++) {
			var child = document.getElementById('circles-' + i),
			percentage = <?php while ($row = mysqli_fetch_array($result1)) { 
				
				echo $row[0] ;
				;
			}?>;
			circles.push(Circles.create({
				id:         child.id,
				value:		percentage,
				radius:     120,
				width:      10,
				colors:     colors[i - 1]
			}));
		}
	</script>	

</body></html>