<!DOCTYPE html>
<!-- saved from url=(0074)https://fooplugins.github.io/FooTable/docs/examples/component/sorting.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="icon" href="../../favicon.ico">-->

<title>Sorting columns - FooTable</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Prism -->
<link href="./css/prism.css" rel="stylesheet">

<!-- FooTable Bootstrap CSS -->
<link href="./css/footable.bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./css/docs.css" rel="stylesheet">

<script src="./js/demo-rows.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php
include ('conf/conn.php');
include ('content/header2.php');
?>
</head>

<body class="docs" class="margin-top-60">

	<div class="jumbotron">
		<div class="container">
			<img  width="600" class="img-responsive center-block" style="max-width:600px;" src="img/logo_telkom_indonesia__2013_-_.png" />
			<h2><center>Sistem Informasi Data Pelanggan Telkom Banyuwangi.</center></h2>
		

			<div class="container">
  <!-- Trigger the modal with a button -->
  <p class="text-center"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Masuk</button></p>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
          <form role="form" method="post" action="login.php" name="login_form">
              <p> <input type="text" class="form-control input-sm" placeholder="Username" id="username" name="username" /></p>
              <p><input type="password" class="form-control input-sm" placeholder="Password Here" id="password" name="password" /></p>
              <p><button type="submit" class="btn btn-primary">Sign in</button>
              </p>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

			
		</div>
	</div>
	<!-- Content -->
	<div class="container">
		<div class="docs-section">

			<div class="callout callout-info">
				<h4>Catatan</h4>
				<ul>
					<li>
						Berikut merupakan Sistem Informasi Terintegrasi dari PT.Telkom Banyuwangi
					</li>
					<li>
Sistem Informasi ini memiliki fitur utama, yaitu dasbor, pencarian data pelanggan, edit data pelanggan, dan memasukan data pelanggan baru.
					</li>
					<li>
						Aplikasi ini dapat diakses di jaringan Intranet lingkungan Telkomsel Banyuwangi dengan harapan dapat membantu Stakeholder Telkom Banyuwangi terutama bagian Consumer Service dalam menentukan pelanggan mana yang potensial.
									</li>
				</ul>
			</div>
	

			<!-- Placed at the end of the document so the pages load faster -->
			<script src="./js/jquery.min.js"></script>
			<script src="./js/bootstrap.min.js"></script>
			<script src="./js/prism.js"></script>
			<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
			<script src="./js/ie10-viewport-bug-workaround.js"></script>
			<!-- Add in any FooTable dependencies we may need -->
			<script src="./js/moment.min.js"></script>
			<!-- Add in FooTable itself -->
			<script src="./js/footable.js"></script>
			<!-- Initialize FooTable -->
			<script>
			jQuery(function($){
				$('#datamain').footable();
			});
			</script>

		</body></html>