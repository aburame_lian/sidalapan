<?php 
session_start();
if($_SESSION['user_login'] != NULL){
    include ('content/header.php');
} else { include ('content/header2.php');}

?>


<!DOCTYPE html>
	<!-- saved from url=(0074)https://fooplugins.github.io/FooTable/docs/examples/component/sorting.html -->
	<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!--<link rel="icon" href="../../favicon.ico">-->

	<title>Sistem Informasi Pelanggan Telkom Banyuwangi</title>

	<!-- Bootstrap core CSS -->
	<link href="./css/bootstrap.min.css" rel="stylesheet">
	<link href="./css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Prism -->
	<link href="./css/prism.css" rel="stylesheet">

	<!-- FooTable Bootstrap CSS -->
	<link href="./css/footable.bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="./css/docs.css" rel="stylesheet">
	<script src="./js/circles-master/circles.js"></script>
	<title>Circles</title>
	<style>
		#canvas .circle {
			display: inline-block;
			margin: 1em;
		}
		.circles-decimals {
			font-size: .4em;
		}
	</style>

	<script src="./js/demo-rows.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	<?php
	?>
	</head>
	<body>
		<!-- Page Content -->
    <div class="container">

        <!-- Introduction Row -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">SIDALAPAN
                    <small>Sistem Informasi Dashboard dan Pengeolaan Pelanggan</small>
                </h1>
                <p>Aplikasi Sistem Informasi Dashboard dan Pengeolaan Pelanggan (SIDALAPAN) merupakan aplikasi yang menampilkan dashboard informatif yang berisikan overview dari keseluruhan data pelanggan serta memiliki fungsi untuk mengelola data pelanggan, seperti menambah, menghapus, dan melakukan perubahan pada data </p>

                <p>
                Tujuan aplikasi ini adalah:
                <li>Membuat suatu wadah untuk mempermudah menentukan pelanggan yang dapat dipromosikan untuk melakukan upgrade layanan.</li>
				<li>Menciptakan wadah yang berfungsi untuk melihat ketercapaian proses upgrade layanan pelanggan.</li>
				<li>Membuat sebuah tabel laporan yang berisikan data pelanggan dan ketercapian proses upgrade layanan pelanggan.</li>
	            </p>
            </div>
        </div>

        <!-- Team Members Row -->
        <div class="row" >
            <div class="col-lg-12 ">
                <h2 class="page-header">Our Team</h2>
            </div>
            <div class="col-lg-6 col-sm-2 text-center ">
                <center><img class="img-circle img-responsive img-center" src="img/img1.JPG" alt=""></center>
                <h3>Muhammad Hafiz Egan Pradana
                    <small>5213100089</small>
                </h3>
                <p>CP: +6282233145231<br>
                Email: hafizegan@gmail.com

                </p>
            </div>
            <div class="col-lg-6 col-sm-2 text-center">
                <center><img class="img-circle img-responsive img-center" src="img/img2.JPG" alt=""></center>
                <h3>Valliant Ferlyando
                    <small>5213100094</small>
                </h3>
                <p>CP: +6281938614512<br>
                Email: aburame.lian1995@gmail.com</p>
            
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <center><p>Copyright &copy; SIDALAPAN Team 2016</p></center>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	</body>
	</html>