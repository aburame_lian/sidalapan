<?php
session_start();
include ('conf/conn.php');
$nama = $_POST['nama'];
$location=$_POST['location'];
$snrmin = $_POST['snrmin'];
$snrmax = $_POST['snrmax'];
$odp= $_POST['odp'];



?>
<!DOCTYPE html>
<!-- saved from url=(0074)https://fooplugins.github.io/FooTable/docs/examples/component/sorting.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<!--<link rel="icon" href="../../favicon.ico">-->

<title>Sorting columns - FooTable</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Prism -->
<link href="./css/prism.css" rel="stylesheet">

<!-- FooTable Bootstrap CSS -->
<link href="./css/footable.bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./css/docs.css" rel="stylesheet">

<script src="./js/demo-rows.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php

	include ('content/header.php');
	$sql = "SELECT `SND_GROUP` as id,`NAMA` as nama,`SNR_UP` as snrup,`SNR_DOWN` as snrdown,`DP` as odp,CONCAT(`LVOIE`,' ',`NVOIE`,' ',`LQUARTIER`) as alamat,`ABONEMEN` as 'abonemen',`CP` as 'cp',`GEOTAG` as 'geotag',`STATUS2` as 'status' FROM `plg_banyuwangi` WHERE NAMA LIKE '%$nama%' and SNR_DOWN<$snrmax and SNR_UP>$snrmin AND CONCAT(`LVOIE`,' ',`NVOIE`,' ',`LQUARTIER`) LIKE '%$location%' AND DP LIKE '%$odp%'";
	$result = mysqli_query($con, $sql)  or die('Query fail: Gagal koneksi DB');
	?>
</head>

<body class="docs" class="margin-top-60">

	
	<!-- Content -->
	<div class="container">
		<div class="docs-section">

			<div class="callout callout-info">
				<h4>Hasil Pencarian</h4>
				<ul>
					<li>
						Berikut merupakan hasil pencarian dari pelanggan dengan nama <?php echo $nama ?> dengan nilai SNR Minimum <?php echo $snrmin ?> hingga nilai SNR Maksimum sebesar <?php echo $snrmax ?> yang memiliki alamat rumah <?php echo $location ?> dengan cluster ODP <?php echo $odp ?>
					</li>
				</ul>	
			</div>
			
			<div class="tab-content">
				
				<?php if($_SESSION['role'] == 'admin'){
					echo '<a href="owner.php" class="btn btn-primary btn-lg">Kembali</a>';
				} else if($_SESSION['role'] == 'user')
				{ 
					echo '<a href="sales.php" class="btn btn-primary btn-lg">Kembali</a>';
				}
				else{

					echo 'salah';
				} ?>
				<button type="button" class="btn btn-primary btn-lg" onclick="printDiv('data')">Print</button>
				</div>

				
				<div class="tab-pane-active" id="data">
					<div class="example">
						<table id="datamain" class="table footable footable-1 breakpoint-lg" data-filtering="true" data-paging="false" data-sorting="true" style="display: table;">
							<thead>
								<tr>
									<th data-breakpoints="xs" data-type="number">ID</th>
									<th>Name</th>
									<th>SNR Margin Up</th>
									<th >SNR Margin Down</th>
									<th >ODP Location</th>
									<th data-breakpoints="xs" >Alamat</th>
									<th data-breakpoints="xs" >Abonemen</th>
									<th data-breakpoints="xs" >CP</th>
									<th data-breakpoints="xs" >Geotag</th>
									<th data-breakpoints="xs" >Status</th>
								</tr>

							</thead>
							<tbody id="printdata">
								<?php while ($row = mysqli_fetch_array($result)) { 
									echo "<tr>";
									echo "<td>" . $row[0] . "</td>\n";
									echo "<td>" . $row[1] . "</td>\n";
									echo "<td>" . $row[2] . "</td>\n";
									echo "<td>" . $row[3] . "</td>\n";
									echo "<td>" . $row[4] . "</td>\n";
									echo "<td>" . $row[5] . "</td>\n";  
									echo "<td>" . $row[6] . "</td>\n";  
									echo "<td>" . $row[7] . "</td>\n";  
									echo "<td>" . $row[8] . "</td>\n";  
									echo "</tr>" ;
								}?>
							</tbody>
						</table>
					</div>

					



			</div> <!-- /container -->

			<!-- Placed at the end of the document so the pages load faster -->
			<script src="./js/jquery.min.js"></script>
			<script src="./js/bootstrap.min.js"></script>
			<script src="./js/prism.js"></script>
			<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
			<script src="./js/ie10-viewport-bug-workaround.js"></script>
			<!-- Add in any FooTable dependencies we may need -->
			<script src="./js/moment.min.js"></script>
			<!-- Add in FooTable itself -->
			<script src="./js/footable.js"></script>
			<!-- Initialize FooTable -->
			<script>
			jQuery(function($){
				$('#datamain').footable();
			});
			
			</script>
			<script type="text/javascript">
				function printDiv(divName) {
     		var printContents = document.getElementById(divName).innerHTML;
     		var originalContents = document.body.innerHTML;

     		document.body.innerHTML = printContents;

    		 window.print();

    		 document.body.innerHTML = originalContents;}
			</script>

		</body></html>