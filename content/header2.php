 <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-container">
                    <span class="sr-only">Show and Hide the Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">
                    <img class="brand" style="max-width:75px; margin-top: -20px;" src="img/logo_telkom_indonesia__2013_-_.png" />
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-container">
                <ul class="nav navbar-nav">
     
                    <li><a href="about-us.php"><span class="glyphicon glyphicon-book"></span> About Us</a></li>
                    
                </ul>
                <form class="navbar-form navbar-left" role="form" method="post" action="login.php">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-user"></span>
                        </span>
                        <input type="text" class="form-control input-sm" placeholder="Username" id="username" name="username" />
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                        </span>
                        <input type="password" class="form-control input-sm" placeholder="Password Here" id="password" name="password" />
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Login</button>
                </form>
            </div>
        </div>
    </nav>